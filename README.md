# st++
Fork of [Suckless terminal emulator version 0.8.1](https://git.suckless.org/st/) with more futures.

<p align="center">
  <img src="screenshot.png?raw=true" alt="Main screenshot"/>
</p>

### What have done
- Add icon support
- Alpha patch enable by default
- Cursor hide patch enable by default
- Mouse wheel patch enable by default
- By default font is set to Ubuntu Mono 15pt
- By default size of window now is 111x34 (cols and rows)
- By right click st++ may open links or dirs or something with xdg-open (can be configure throught config.h)

### Goals
- Split code
- Fix render bug with mouse wheel patch
- Since I like to customize my desktop offten, I think I need configuration file

### Depends
- libxft-dev
- libx11-dev
- libfontconfig1-dev
- libfreetype6-dev
- libxrender-dev
- libgd-dev

### Icon search algorithm
Basicly icon search in three places by default (in that order):
/usr/share/icons/hicolor/24x24/apps/
/home/$USER/.icons/CURRENT_THEME/22x22/apps/
/home/$USER/.icons/

Icon must be named terminal.png (small register).
If icon can't be found, will be used default (window manager) icon.

### Dynamic title
In ~/.bashrc at very end:

```bash
trap 'echo -ne "\033]0;Terminal\007"' SIGHUP SIGINT SIGTERM
trap 'echo -ne "\033]0;Terminal - ${BASH_COMMAND}\007"' DEBUG
```

### Break stuff

During refactoring I 'may' break some stuff. For example I break dstat programm when trying implement new sequence. So I add MAYBEBREAK commentary when I don't sure.

### Credits
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

Use some xseticon code by LeoNerd (I delete all glib stuff).
